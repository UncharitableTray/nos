#!/bin/bash

# Ova skripta demonstrira kako se koristi program i pokrece nekoliko testnih primjera
# od kojih su neki takoder bitni i za setup ostalih eksperimenata (npr. generiranje parova kljuceva)

# Prvo je potrebno izgraditi izvrsnu datoteku naredbom 'cargo build --release'
cargo build --release
cp target/release/crypto ./crypto
echo "Target built for release and copied to current working directory"

# Program se uvijek pokrece naredbom './crypto', koju slijede znakovi '--' i ostatak parametara/naredbi.
# Popis istih moze se vidjeti pokretanjem naredbe './crypto help', ali ukratko, podrzani su:

# help                                              # pokazuje mali pomocni ispis
# gen <sec_key> <pub_key.pub>                       # generira par RSA kljuceva i pohrani ih u datoteke 'sec_key' i 'pub_key.pub'
# sign <my_sec> <message_file>                      # potpisuje poruku iz 
# verify <their_pub.pub> <signed_file>              # provjerava potpis iz datoteke koja sadrzi potpis
# encrypt <their_pub.pub> <plaintext_file>          # kriptira datoteku (potreban je javni kljuc osobe kojoj saljemo poruku)
# decrypt <my_sec> <ciphertext_file>                # dekriptira datoteku (potreban je privatni kljuc osobe koja je primila poruku)
# seal <my_sec> <their_pub.pub> <plaintext_file>    # pecatira datoteku (potreban je privatni kljuc osobe koja salje i javni osobe koja prima)
# unseal <my_sec> <their_pub.pub> <sealed_file>     # otvara pecat (potreban je privatni kljuc osobe koja prima i javni osobe koja salje)

# Takoder je moguce uz svaku naredbu pozvati i help:
# ./crypto help <naredba>
# npr.
# ./crypto help seal

# Osim tih naredbi, podrzani su i mnogi parametri koji se navode ispred naredbi:
# --duganaredba/-kratica <vrijednost>

# --symalg/-s [AES, BF]                     (default: AES)      # simetricni algoritam
# --symkey/-k [128, 192, 256]               (default: 256)      # duljina simetricnog kljuca
# --symmod/-m [ECB, CBC, OFB, CTR]          (default: CBC)      # nacin rada simetricnog algoritma
# --asyalg/-a [RSA]                         (default: RSA)      # trenutno je samo RSA podrzan
# --asylen/-l [1024, 2048, 3072, 4096]      (default: 2048)     # duljina RSA kljuca
# --hashfn/-h [MD5, SHA1, SHA224, SHA256, SHA384, SHA512, SHA3_224, SHA3_256, SHA3_384, SHA3_512] (default: SHA3_512)

# Ako ne zelimo koristiti defaultne vrijednosti, parametri se navode prije naredbi
# npr. './crypto -s BF -k 192 -m ECB -h SHA3_256 seal my_sec their_pub plaintext_file'

# NAPOMENA!! Zbog ogranicenja u Rust bindinga na Openssl biblioteku, nisu podrzane sve kombinacije
# simetricne enkripcije, nacina rada i duljine kljuca. Podrzano je sljedece:
# AES {128, 192, 256} {ECB, CBC, CFB, OFB, GCM}
# BF {128, 192, 256} {ECB, CBC, OFB}
# Takoder, zbog istog razloga sam morao zamijeniti 3DES nekim drugim algoritmom, pa sam odabrao
# Schneierov Blowfish (BF parametar)


# ===================================
# Prvo zelimo generirati dva para RSA kljuceva (naredba 'gen') koji ce biti pohranjeni u direktoriju 'keys'
# Podrzane duljine kljuceva su 1024, 2048, 3072, 4096 (default: 2048, ako je parametar -l izostavljen)

mkdir keys
KLEN=1024
echo "Generating the key pairs into the keys directory."
./crypto -l $KLEN gen keys/my_sec keys/my_pub.pub
echo "My keys saved as (my_sec, my_pub.pub)"
./crypto -l $KLEN gen keys/their_sec keys/their_pub.pub
echo -e "Their keys saved as (their_sec, their_pub.pub)\n"


# ===================================
# Mozemo testirati potpisivanje i verifikaciju
# npr. ja potpisujem vlastitu poruku:

INPUT_FILE=my_plaintext.txt     # Mozete promijeniti ovu datoteku ako imate vlastiti input.
HASH=SHA3_384
./crypto -h $HASH sign keys/my_sec $INPUT_FILE > my_signed.txt
echo "Message signed and saved in 'my_message.txt'"
# Drugi user moze verificirati moju potpisanu poruku
./crypto -h $HASH verify keys/my_pub.pub my_signed.txt
echo ""


# ===================================
# Mozemo testirati kriptiranje i dekriptiranje
# Ja saljem svoju poruku kriptiranu nekom drugom korisniku, za sto mi treba njegov javni kljuc
# Session key se generira automatski i appenda u poruku:

SIMALG=AES
SIMLEN=256
MODE=OFB
./crypto -s $SIMALG -k $SIMLEN -m $MODE encrypt keys/their_pub.pub $INPUT_FILE > my_encrypted.txt
echo "Successfully encrypted my plaintext file (result in my_encrytped.txt)"
./crypto -s $SIMALG -k $SIMLEN -m $MODE decrypt keys/their_sec my_encrypted.txt > my_decrypted.txt
echo "Successfully encrypted my ciphertext file (result in my_decrypted.txt)"
diff $INPUT_FILE my_decrypted.txt && echo -e "No differences! I.e. plaintext and decrypted files are the same!\n"


# ===================================
# Ista stvar vrijedi i za pecat:
# Zelim kriptirati moj plaintext i potpisati ga
# Ovaj put koristimo 192-bit Blowfish u CBC modu:

SIMALG=BF
SIMLEN=192
MODE=CBC
./crypto -s $SIMALG -k $SIMLEN -m $MODE seal keys/my_sec keys/their_pub.pub $INPUT_FILE > my_sealed.txt
echo "Successfully sealed my plaintext file (result in my_sealed.txt)"
./crypto -s $SIMALG -k $SIMLEN -m $MODE unseal keys/their_sec keys/my_pub.pub my_sealed.txt > my_unsealed.txt
echo "Successfully unsealed my sealed file (result in my_unsealed.txt)"
diff $INPUT_FILE my_unsealed.txt && echo "No differences! I.e. plaintext and unsealed files are the same!"


# ===================================
# Konacno, sve dobivene datoteke mogu se pocistiti sljedecom naredbom:
# make clean

# Znam da je kod dosta neuredan i alat je malo zapetljan, nadam se da je ova datoteka barem donekle pomogla.
