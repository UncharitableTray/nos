extern crate carousel;
extern crate posixmq;
extern crate rand;

use std::collections::HashMap;
use std::io::ErrorKind;
use std::process::Command;
use std::{thread, time};

use carousel::{
    calc_mq_name, Visitor, CAPACITY, DONE, NUM_VISITORS, REQUEST, ROUND_START, ROUND_STOP, SIT,
    STAND,
};
use posixmq::OpenOptions;
use rand::{thread_rng, Rng};

fn main() {
    let mut children = Vec::new();
    let mut visitors = HashMap::new();

    // Za svakog visitora imam dva reda jer sam imao puno problema sa sinkronizacijom
    // citanja i pisanja, tj. 
    for i in 0..NUM_VISITORS {
        // Pomocu ove Command strukture pokrecem posjetitelje kao posebne procese
        let child = Command::new("./target/release/visitor")
            .arg(i.to_string()) // njihov indeks
            .arg(std::process::id().to_string()) // moj PID (indeks + PID se koriste za ime opisnika reda)
            .spawn()
            .expect("Failed to execute. Check starting root directory and build in release mode.");
        children.push((i, child));
        let mq_wname = calc_mq_name(std::process::id() as usize, i, 's');   // Stvara string za opisnik reda
        let mq_write = OpenOptions::writeonly()
            .capacity(5)
            .max_msg_len(200)
            .create_new()
            .open(&mq_wname)
            .expect("Unable to create MQ");

        let mq_rname = calc_mq_name(std::process::id() as usize, i, 'c');
        let mq_read = OpenOptions::readonly()
            .capacity(5)
            .max_msg_len(200)
            .create_new()
            .open(&mq_rname)
            .expect("Unable to create MQ");

        visitors.insert(
            i,
            Visitor {
                id: i,
                mq_write: mq_write,
                mq_read: mq_read,
                mq_wname: mq_wname,
                mq_rname: mq_rname,
            },
        );
    }

    let mut sitting: Vec<usize> = Vec::new();
    let mut msgbuf = [0; 200];
    while !visitors.is_empty() {
        // Ako sjede 4 posjetitelja, kreni
        // Ovdje sam pretpostavio da ce broj posjetitelja
        // uvijek biti djeljiv kapacitetom, ali vjerojatno se moze
        // dodati jos jedan uvjet koji provjerava je li broj
        // posjetitelja ukupnih jednak broju onih koji sjede
        if sitting.len() == CAPACITY {
            println!("{}", ROUND_START);
            let duration = thread_rng().gen_range(1000, 3001);
            //println!("Schleeping for... {}", duration);
            thread::sleep(time::Duration::from_millis(duration));
            println!("{}", ROUND_STOP);
            for id in sitting.iter() {
                let sitter = visitors.get(id).unwrap();
                sitter
                    .mq_write
                    .send(0, &STAND.as_bytes())
                    .expect("Unable to send STAND");
            }
            sitting.clear()
        }

        // Pomocna lista posjetiteljia koji se vise ne zele vrtiti
        // Ne izbacujem direktno iz mape posjetitelja jer je losa
        // praksa petljati po kolekciji tijekom iteriranja po istoj
        // (sto mi compiler ni ne dozvoljava)
        let mut donezos: Vec<usize> = Vec::new();
        for (v_id, v) in visitors.iter() {
            if sitting.len() == CAPACITY {
                //println!("Breaking!");
                break;
            }
            if v.mq_read.attributes().current_messages == 0 {
                continue;
            }
            match v.mq_read.receive(&mut msgbuf) {
                Err(ref e) => {
                    // Nema poruke, preskacemo taj slucaj
                    if e.kind() == ErrorKind::WouldBlock {
                        println!(
                            "No message from user {}... msgcount {}",
                            v_id,
                            v.mq_read.attributes().current_messages
                        );
                        continue;
                    } else {
                        // Neki drugi error na koji nisam racunao:
                        panic!("Error receiving message: {}", e);
                    }
                }
                // Stigla je ok poruka
                Ok((_, len)) => {
                    let msg = String::from_utf8_lossy(&msgbuf[..len]);
                    // Ako je zahtjev za vrtuljkom
                    if msg == REQUEST {
                        sitting.push(v.id);
                        v.mq_write
                            .send(0, &SIT.as_bytes())
                            .expect("Unable to send SIT message");
                    // Ako je korisnik gotov s vrtuljkom
                    } else if msg == DONE {
                        donezos.push(*v_id);
                    }
                }
            };
        }

        // Ovdje cistim posjetitelje koji su gotovi, tj. zatvaram njihove redove
        for id in donezos.iter() {
            let name = &visitors.get(&id).unwrap().mq_wname;
            posixmq::unlink(&name).expect("Unable to close msg write queue");
            let name = &visitors.get(&id).unwrap().mq_rname;
            posixmq::unlink(&name).expect("Unable to close msg read queue");
            visitors.remove(&id);
        }
    }

    for (_, child) in children.iter_mut() {
        child.wait().expect("Unable to wait for child, check why?");
    }
}
