extern crate posixmq;

use posixmq::PosixMq;

pub const NUM_VISITORS: usize = 8;
pub const CAPACITY: usize = 4;

pub static REQUEST: &str = "Zelim se voziti!";
pub static SIT: &str = "Izvoli, sjedi.";
pub static STAND: &str = "Molim te, ustani.";
pub static DONE: &str = "Ja sam gotov!";

pub static ROUND_START: &str = "Pokrecem vrtuljak.";
pub static ROUND_STOP: &str = "Zaustavljam vrtuljak.";

pub struct Visitor {
    pub id: usize,
    pub mq_write: PosixMq,
    pub mq_read: PosixMq,
    pub mq_wname: String,
    pub mq_rname: String,
}

pub fn calc_mq_name(parent_id: usize, child_id: usize, qtype: char) -> String {
    let mut mq_name = String::from("/");
    mq_name.push_str(&parent_id.to_string());
    mq_name.push('-');
    mq_name.push_str(&child_id.to_string());
    mq_name.push(qtype);
    mq_name
}
