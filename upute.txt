Programi su pisani u Rustu. Iako je jezik portabilan, konkretne biblioteke za redove/cjevovode nisu
tako da bi trebalo raditi samo na UNIX sustavima (ili GNU/Linux, ne znam tocno)
Ako je instaliran toolchain, pokrecu se ovako:


Vjezba 1. a) Redovi poruka:
      I. `cd ./carousel`            #prebacivanje u root direktorij projekta
     II. `cargo build --release`    #prevodenje programa uz release optimizacije !! bitno da bude release jer iz release direktorija pokrecem procese rucno
    III. `cargo run --bin carousel` #pokrece vrtuljak koji je ujedno i dispatcher posjetitelja


Vjezba 1. b) Cjevovodi:
      I. `cd ./philosophers`
     II. `cargo build --release`    #ovdje --release flag nije pretjerano bitan ali brze ce raditi valjda
    III. `cargo run -- <N>`                #nije potrebno specificirati ime binary datoteke jer postoji samo jedna - main.rs (ona Vas vjerojatno najvise zanima, tamo je sva logika), a <N> predstavlja broj cvorova, tj filozofa
