from PIL import Image, ImageDraw
import itertools
from functools import reduce
import operator


def all_factors(prime_dict):
    series = [[p**e for e in range(maxe+1)] for p, maxe in prime_dict.items()]
    for multipliers in itertools.product(*series):
        yield reduce(operator.mul, multipliers)


# logo-enc.txt sadrzi ciphertext kriptirane slike
with open("logo-enc.txt") as file:
    ciphertext = file.read().strip()

# for t in textwrap.wrap(data, 32):
#    print(t)

hex_data = bytes.fromhex(ciphertext)

# velicina logo-enc.txt fajla je 691489B, -1B za \n pa dobijemo faktorizaciju
# iz nje se onda daju generirati parovi (x, y) t.d. x*y = velicina koje
# koristimo kao dimenzije za sliku
prime_dict = {2: 2, 3: 2, 7: 4}
L = sorted(all_factors(prime_dict))
number_of_divisors = reduce(lambda prod, e: prod*(e+1), prime_dict.values(), 1)

n, isodd = divmod(len(L), 2)
for x in (zip(L[:n], reversed(L[n + isodd:]))):
    print(x)
    img = Image.frombuffer('RGBA', (x[0], x[1]), hex_data)
    draw = ImageDraw.Draw(img)
    img.save(str(x[0]) + "x" + str(x[1]) + "_slika.bmp")
    img = Image.frombuffer('RGBA', (x[1], x[0]), hex_data)
    draw = ImageDraw.Draw(img)
    img.save(str(x[1]) + "x" + str(x[0]) + "_slika.bmp")
