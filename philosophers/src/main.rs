extern crate os_pipe;
extern crate philosophers;
extern crate rand;

use std::collections::{BinaryHeap, HashMap};
use std::env;
use std::{thread, time};

use os_pipe::pipe;
use philosophers::{IndexedPipe, Message, Philosopher, IZLAZAK, ODGOVOR, ZAHTJEV};
use rand::{thread_rng, Rng};


// Ovdje je rjesenje za sinkronizaciju Lamportovim raspodijeljenim protokolom
// Vecina bitne logike je u ovom fileu, ali sve pomocne strukture
// su definirane u ./lib.rs
// Za one koji imaju rust toolset instaliran, labos se pokrece naredbom
// `cargo run -- [N]`

fn main() {
    let args: Vec<String> = env::args().collect();
    let n = args[1].parse::<usize>().unwrap();

    let mut philosophers = Vec::new();
    for i in 0..n {
        philosophers.push(Philosopher {
            id: i,
            n: n,
            pipes: HashMap::new(),
        });
    }

    /// Svaki filozof otvara dva cjevovoda po drugom filozofu j
    /// Dosta slozeno rjesenje po broju cjevovoda ali mi se cinilo najjednostavnije
    /// za slucaj bez nekog centralnog cvora koji distribuira poruke dalje
    /// 
    /// Nigdje eksplicitno ne zatvaram cjevovode, to se dogada implicitno
    for i in 0..n {
        for j in 0..n {
            /// Reader je kraj iz kojeg se citaju podaci, writer je kraj u koji se pisu
            let (my_reader, their_writer) = pipe().expect("Unable to create pipe");
            let (their_reader, my_writer) = pipe().expect("Unable to create pipe");

            let mine_to_theirs = IndexedPipe::new(j, my_reader, my_writer);
            let theirs_to_mine = IndexedPipe::new(i, their_reader, their_writer);

            philosophers
                .get_mut(i)
                .unwrap()
                .pipes
                .insert(j, mine_to_theirs);
            philosophers
                .get_mut(j)
                .unwrap()
                .pipes
                .insert(i, theirs_to_mine);
        }
    }

    let mut handles = Vec::new();
    for mut phil in philosophers {
        // koristim threadove jer fork u jeziku ne postoji
        // jedino sto imam je Command/Child kojeg sam koristio
        // u proslom zadatku, ali pomocu njega ne mogu dijeliti
        // cjevovode procesima, nego samo namjestiti read/write/error
        let handle = std::thread::spawn(move || {
            let my_id = phil.id;
            let mut my_time = thread_rng().gen_range(100, 2001);
            let mut prio_queue = BinaryHeap::new();

            let request = Message::new(my_id, my_time, ZAHTJEV);
            prio_queue.push(request.clone());
            // Zahtjev saljem svim ostalim cvorovima
            for j in 0..phil.n {
                if j == phil.id {
                    continue;
                }
                phil.send(j, &request.to_string());
            }

            let mut me_done = false;
            let mut done_counter = 0;
            let mut replies = Vec::new();
            thread::sleep(time::Duration::from_millis(my_time as u64));
            loop {
                if done_counter == phil.n {
                    break;
                }

                let first = prio_queue.peek().expect("No more elements left");
                if !me_done && replies.len() == n - 1 && my_id == first.index {
                    println!("Filozof {} je za stolom!", my_id);
                    thread::sleep(time::Duration::from_secs(3));
                    let end = Message::new(my_id, my_time, IZLAZAK);
                    done_counter += 1;
                    me_done = true;

                    for k in 0..phil.n {
                        if k == my_id {
                            continue;
                        }
                        phil.send(k, &end.to_string());
                        replies.clear();
                    }
                    continue;
                }

                for j in 0..phil.n {
                    if j == my_id {
                        continue;
                    }

                    let full_text = phil.read(j);
                    // Nekad se dogodi da se u isti pipeline od istog cvora
                    // nagura nekoliko poruka, njih odvajam znakom :
                    // a dijelove same poruke odvajam znakom ;
                    // Malo je neuredno, znam...
                    let msgs = full_text
                        .split(':')
                        .map(|x| parse_message(x.to_string()))
                        .collect::<Vec<Option<Message>>>();
                    for msg in msgs {
                        // msg ce biti none kada je dobiveni text duljine 0 nakon splita
                        // to se dogodi na kraju programa ili nekad tijekom obrade
                        // ovaj continue je dodan radi robusnosti neke
                        if msg.is_none() {
                            continue;
                        }
                        let msg = msg.unwrap();

                        // Ako je dosao odgovor, zabiljezi koji cvor je odgovorio
                        // Ovaj podatak zapravo nigdje ne koristim, nego duljinu
                        // vektora koristim kao glorified brojac...
                        if msg.msg == ODGOVOR {
                            replies.push(msg.index);
                        // Ako je stigao zahtjev, odmah odgovori na njega i dodaj u prioritetni red
                        } else if msg.msg == ZAHTJEV {
                            prio_queue.push(msg.clone());
                            my_time = my_time.max(msg.timestamp) + 1;
                            let reply = Message::new(my_id, my_time, ODGOVOR);
                            phil.send(j, &reply.to_string());
                        // Ako je poruka izlaska, izbaci prvi jer se ionako racuna da ce on jedini
                        // imati pravo uci u kriticni odsjecak
                        } else if msg.msg == IZLAZAK {
                            prio_queue.pop().expect("Unable to pop queue");
                            done_counter += 1;
                        // Doslo je do neke greske u parsiranju
                        } else {
                            panic!("Message {:?} is neither of the three options!", msg.msg);
                        }
                    }
                }
            }
            let new_time = thread_rng().gen_range(100, 2001);
            thread::sleep(time::Duration::from_millis(new_time as u64));
            println!("Filozof {} je napokon procitao Fenomenologiju duha", my_id);
            // ^ ova izjava je vjerojatno laz...
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }
}

fn parse_message(input: String) -> Option<Message> {
    if input.len() == 0 {
        return None;
    }
    let parts: Vec<&str> = input.split(';').collect();
    let their_id = parts[0].parse::<usize>().expect("Unable to parse index");
    let timestamp = parts[1]
        .parse::<usize>()
        .expect("Unable to parse timestamp");
    Some(Message::new(their_id, timestamp, parts[2]))
}
